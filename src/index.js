'use strict';

import express from 'express';
import cors from 'cors';
import winston from 'winston';
import tracer from 'tracer';
import apiRouter from './routes/snippets';
import projectRouter from './routes/projects';
import fetchProjectRouter from './routes/fetch-projects';
import { initTestDatastore } from './lib/datastore';
import "babel-core/register";
import "babel-polyfill";

const log = tracer.console();

var startupTime;
const app = express();
app.set('json spaces', 2);

app.get('/', (req, res) => {
  let status = {
    version: process.env.GAE_VERSION || 'local',
    startup_time: startupTime
  }
  res.status(200).json(status);
});

app.use(cors());

app.use('/editor', express.static('build'));
app.use('/snippets', apiRouter);
app.use('/projects', projectRouter);
app.use('/fetch-projects', fetchProjectRouter);

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' })
  ]
});

const server = app.listen(process.env.PORT || 8082, () => {
  startupTime = new Date(Date.now()).toLocaleString();
  
  if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
      format: winston.format.simple()
    }));

    logger.info('Listening on port ' + server.address().port);

    initTestDatastore().catch(err => {
      logger.error('cannot initialize datastore for testing: '+ err);
    })
  }
});

export default app;