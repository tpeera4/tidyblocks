'use strict';

import express from 'express';
import {retrieveProjects} from '../lib/project-fetch';
var router = express.Router();


router.get('/', async function(req, res){
    let offset = req.query.offset||0;
    let num_projects = req.query.num_projects||1000;
    let mode = req.query.mode||'trending';
    const projects = await retrieveProjects(offset, num_projects,mode);
    res.json(projects);
});

export default router;