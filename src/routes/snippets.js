'use strict';

import express from 'express';
import { retrievePassword } from '../lib/datastore';
import { getEsData } from '../lib/elasticsearch';
import "babel-core/register";
import "babel-polyfill";

var router = express.Router();

router.get('/', async (req, res, next) => {
  const es_pwd = await retrievePassword().catch(next);
  const data = await getEsData(es_pwd).catch(next);
  res.status(200).json(data);
});

router.get('/:id', async (req, res, next) => {
  const data = {
    _id: req.params['id'],
    _source: {
      description: "desc",
      xml : "xml"
    }
  }||await getEsData(es_pwd).catch(next);
  res.status(200).json(data);
});

export default router;